<!DOCTYPE html>
<html lang="en">
<head>
  <title>ABBC WALLET</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
  <script src="{{ asset('js/webcam.js') }}"></script>



  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>


  
  
    <div class="container main-container">
    <div class="row">


    </div>
  
   <div class="row-modal" id="login-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-heading" align="center">

            <img class="login-logo" src="{{ asset('img/abbc.svg') }}">
            <h3>WALLET</h3>
            <img class="login-line" src="{{ asset('img/line.svg') }}">
          </div>

          <div class="modal-body" align="center">
            
              <div class="form-group">
                <div class="input-group login-textfield">
                   <div id="my_camera"></div>
                </div>
              </div>
          

            
            <div class="form-group text-center login-button">
             
            
				   <a href="/signup" type="submit" class="btn btn-sm btn-login" >Sign Up</a>
            
            </div>


            <div class="form-group text-center login-button">
           
             
						<a href="/login" type="submit" class="btn btn-sm btn-login"  >login</a>
           
            </div>



          </div>
        </div>
      </div>
    </div>

  </div>


  
  
  
  



 <div id="footer" class="fixed-footer">
    <div class="container">
      <a href="https://abbcfoundation.com/"><p class="footer-block">www.abbcfoundation.com</a> &copy; All Rights Reserved.</p>

      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="#">Privacy</a>
        </li>
        <li class="list-inline-item">
          <a href="#">Terms</a>
        </li>
        <li class="list-inline-item">
          <a href="#">FAQ</a>
        </li>
      </ul>
    </div>
  </div>
   <script language="JavaScript">
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90,
            force_flash:false
        });
        Webcam.attach( '#my_camera' );
    </script>
	
	   <!-- Code to handle taking the snapshot and displaying it locally -->
    <script language="JavaScript">
        var base64 = '';
        function take_snapshot() {
            // take snapshot and get image data
            Webcam.snap( function(data_uri) {
                console.log(data_uri);
                $("#image").val(data_uri);
                base64 = data_uri;
                // display results in page
                document.getElementById('results').innerHTML =
                    '<img src="'+data_uri+'"/>';
            } );
        }
        $("#form").submit(function (e) {
            signup();
            e.preventDefault();
        });
        function signup() {
            var request = $.ajax({
                url: "{{ asset('faical/recognize.php') }}",
                type: "POST",
                data: {image : base64},
                success:function (response) {
                    console.log(response);
                    response = JSON.parse(response);
                    if(response.hasOwnProperty('images')){
                        var status = response.images[0].transaction.status
                        if(status == "failure"){
                            alert("login failed. please signup");
                        }
                    }
                    if(response.hasOwnProperty('Errors')){
                        if(response.Errors.length >0){
                            var code = response.Errors[0].ErrCode;
                            if(code == 1002){
                                alert("take a snapshot before login");
                            }
                            if(code == 5001 || code == 5002){
                                alert("take snapshot before login");
                            }
                            if(code == 5002){
                                alert("no faces found in the image");
                            }
                        }else{
                            alert("login failed");
                        }
                    }else{
                        console.log("success");
                        var text = "<ul>";
                        response.images[0].candidates.forEach(function (object,index,array) {
                            window.location = ""{{ asset('faical/admin.php') }}"?username="+object.subject_id;
                        });


                    }
                },
                error:function (error) {
                    console.log(error);
                }

            });
        }
    </script>
</body>
</html>
