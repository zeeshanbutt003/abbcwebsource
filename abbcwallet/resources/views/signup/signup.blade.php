<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Signup to your account</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



     <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

</head>
<body>

  <div class="container main-container">
    <div class="row">

    </div>
    <div class="row-modal" id="login-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-heading" align="center">
              <h3>Signup to your account with webcam</h3>
          </div>

          <div class="modal-body" align="center">
            <div class="row">
                <div id="my_camera"></div>
          </div>

          <div class="row">
              <div id="results"></div>
          </div>

          <form id="form">
            <div class="form-group text-center login-button">
                <button class="btn btn-primary btn-login" type = "button" onClick = "take_snapshot()" style="margin-top: 10px">Take Snapshot</button>
            
			
			</div>

            <div class="form-group text-center login-button">
                
				 <button class="btn btn-sm btn-login"  type = "submit" id = "signup">Signup with image</button>
            </div>
        </form>


    </div>
</div>
</div>
</div>

</div>



 <!--   <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8" style="text-align: center">
                <h1>Signup to your account with webcam</h1>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
                <div id="my_camera"></div>

                <form id="form">
                    <button class="btn btn-primary" type = "button" onClick = "take_snapshot()" style="margin-top: 10px">Take Snapshot</button>
                    <button class="btn btn-primary" type = "submit" id = "signup">Signup with image</button>
                </form>
            </div>
            <div class="col-sm-5" >
                <div id="results">Your captured image will appear here...</div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>-->


    <!-- First, include the Webcam.js JavaScript Library -->
  <script src="{{ asset('js/webcam.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Configure a few settings and attach camera -->
    <script language="JavaScript">
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 100,
            force_flash:false
        });
        Webcam.attach( '#my_camera' );
    </script>

        <!-- Code to handle taking the snapshot and displaying it locally -->
    <script language="JavaScript">
        var base64 = '';
        function take_snapshot() {
            // take snapshot and get image data

            Webcam.snap( function(data_uri) {
                console.log(data_uri);
                $("#image").val(data_uri);
                base64 = data_uri;
                // display results in page
                // document.getElementById('results').innerHTML =
                //     '<h2>Here is your image:</h2>' +
                //     '<img src="'+data_uri+'"/>';
                document.getElementById('results').innerHTML =
                    '<img src="'+data_uri+'" align="center"/>';
            } );
        }
        $("#form").submit(function (e) {
           signup();
           e.preventDefault();
        });
        function signup() {
            var request = $.ajax({
                url: "{{ asset('faical/enroll.php') }}",
                type: "POST",
                data: {image : base64,username:$("#username").val()},
                success:function (response) {
                    console.log(response)
                    response = JSON.parse(response);
                    if(response.hasOwnProperty('Errors')){
                        if(response.Errors.length >0){
                            var code = response.Errors[0].ErrCode;
                            if(code == 1002){
                                alert("take a snapshot before login");
                            }
                            if(code == 1005){
                                alert("face is already registered");
                            }
                            if(code == 5002){
                                alert("no faces found in the image");
                            }
                        }
                    }else {
                        alert("successfully signup");
                    }

                },
                error:function (error) {
                    console.log(error);
                }

            });
        }
    </script>
</body>
</html>


