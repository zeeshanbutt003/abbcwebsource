<!doctype html>

<html lang="en">
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Signup to your account</title>
    <style type="text/css">
        body { font-family: Helvetica, sans-serif; }
        h2, h3 { margin-top:0; }
        form { margin-top: 15px; }
        form > input { margin-right: 15px; }
        #usernames { float:right; margin:20px; padding:20px; border:1px solid; background:#ccc; }
        #results{
            width: 320px;
            height: 240px;
            border: 1px solid;
            background:#ccc;
            text-align:center;
            margin-top: auto;
            margin-bottom: auto;
        }

        #my_camera {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        #results {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        #usernames {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8" style="text-align: center">
                <h1>Login to your Abbcfoundation Account</h1>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
                <div id="my_camera"></div>

                <form id="form">
                    <button class="btn btn-primary" type = "button" onClick = "take_snapshot()" style="margin-top: 10px">Take Snapshot</button>
                    <button class="btn btn-primary" type = "submit" id = "signup" style="margin-top: 10px">Login with image</button>
                </form>
                <form id="id">
                    <input type="text" id="userId" placeholder="enter your id">
                    <button class="btn btn-primary" type = "submit" id = "signup" style="margin-top: 10px">Login with Id</button>
                </form>
            </div>
            <div class="col-sm-5" >
                <div id="results">Your captured image will appear here...</div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>

    <!-- First, include the Webcam.js JavaScript Library -->
     <script src="{{ asset('js/webcam.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script language="JavaScript">
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90,
            force_flash:false
        });
        Webcam.attach( '#my_camera' );
    </script>

    <!-- Code to handle taking the snapshot and displaying it locally -->
    <script language="JavaScript">
        $(document).ready(function() {
            var facerec = "";
            $.get("getSetting.php?userId="+getCookie('username'),function (result) {
                facerec = result;
                if(facerec == "0"){
                    $("#form").hide();
                    $("#my_camera").hide();
					
                }else if (facerec == "1"){
                    $("#id").hide();
                }else{
                    $("#id").hide();
                }
            });

        });

            var base64 = '';
        function take_snapshot() {
            // take snapshot and get image data
            Webcam.snap( function(data_uri) {
                console.log(data_uri);
                $("#image").val(data_uri);
                base64 = data_uri;
                // display results in page
                document.getElementById('results').innerHTML =
                    '<img src="'+data_uri+'"/>';
            } );
        }
        $("#form").submit(function (e) {
            signup();
            e.preventDefault();
        });
        $("#id").submit(function (e) {
            var cookies = "username = "+ $("#userId").val()
            document.cookie =cookies;
            console.log(cookies);
            window.location = "admin.php";
            e.preventDefault();
        });
        $(document).ready
        function signup() {
            var request = $.ajax({
                url: "recognize.php",
                type: "POST",
                data: {image : base64},
                success:function (response) {
                    console.log(response);
                    response = JSON.parse(response);
                    if(response.hasOwnProperty('images')){
                        var status = response.images[0].transaction.status
                        if(status == "failure"){
                            alert("face not registered. please signup");
                        }
                    }
                    if(response.hasOwnProperty('Errors')){
                        if(response.Errors.length >0){
                            var code = response.Errors[0].ErrCode;
                            if(code == 1002){
                                alert("take a snapshot before login");
                            }
                            if(code == 5001 || code == 5002){
                                alert("no faces found in the image");
                            }
                            if(code == 5002){
                                alert("no faces found in the image");
                            }
                        }else{
                            alert("login failed");
                        }
                    }else{
                        console.log("success");

                        var text = "<ul>";
                        response.images[0].candidates.forEach(function (object,index,array) {
                            console.log(object.subject_id);
                            var cookie = "username="+object.subject_id;
                            document.cookie = cookie;
//                            object.subject_id;
                            window.location = "admin.php";

                        });
                    }
                },
                error:function (error) {
                    console.log(error);
                }

            });
        }
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    </script>
</body>
</html>
