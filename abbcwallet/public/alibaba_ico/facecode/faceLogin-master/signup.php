<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Signup to your account</title>
    <style type="text/css">
        body { font-family: Helvetica, sans-serif; }
        h2, h3 { margin-top:0; }
        form { margin-top: 15px; }
        form > input { margin-right: 15px; }
        #results { float:right; margin:20px; padding:20px; border:1px solid; background:#ccc; }
    </style>
</head>
<body>
<div id="results">Your captured image will appear here...</div>

<h1>Signup to your account with webcam</h1>
<div id="my_camera"></div>

<!-- First, include the Webcam.js JavaScript Library -->
<script type="text/javascript" src="webcam.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
    Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90,
        force_flash:false
        

    });
    Webcam.attach( '#my_camera' );
</script>

<!-- A button for taking snaps -->
<form id="form">
    <input type=button value="Take Snapshot" onClick="take_snapshot()">
<!--    <input type="text" hidden="hidden" id="image" required>-->
    <input type="text" placeholder="enter username" id="username" required>
    <input type=submit value="Signup with image" id="signup">
</form>

<!-- Code to handle taking the snapshot and displaying it locally -->
<script language="JavaScript">
    var base64 = '';
    function take_snapshot() {
        // take snapshot and get image data
        
        Webcam.snap( function(data_uri) {
            console.log(data_uri);
            $("#image").val(data_uri);
            base64 = data_uri;
            // display results in page
            document.getElementById('results').innerHTML =
                '<h2>Here is your image:</h2>' +
                '<img src="'+data_uri+'"/>';
        } );
    }
    $("#form").submit(function (e) {
       signup();
       e.preventDefault();
    });
    function signup() {
        var request = $.ajax({
            url: "enroll.php",
            type: "POST",
            data: {image : base64,username:$("#username").val()},
            success:function (response) {
                console.log(response)
                response = JSON.parse(response);
                if(response.hasOwnProperty('Errors')){
                    if(response.Errors.length >0){
                        var code = response.Errors[0].ErrCode;
                        if(code == 1002){
                            alert("take a snapshot before login");
                        }
                    }
                }else {
                    alert("successfully signup");
                }

            },
            error:function (error) {
                console.log(error);
            }

        });
    }
</script>

</body>
</html>
