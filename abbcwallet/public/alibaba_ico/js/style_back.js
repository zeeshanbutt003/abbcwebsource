$(document).ready(function() {
// KB 빅데이터

// 서브네비게이션 슬라이딩
$(".btn_subnavi").click(function() {
	$("body").toggleClass("off");
});

// 베너 슬라이딩
$(".wrap_banner").each(function() {
	var lis = $(".banner_cont_inner li");
	for (i=0; i< lis.length ; i++ ){
		var lis_el = '<span><a href="#' +i +'">'+i+'</a></span>';
		$(".banner_page").append(lis_el);
	}
	var emtWidth = $(".banner_cont").outerWidth();
	var emtHeight = $(".banner_cont").outerHeight();
	//alert(emtWidth)
	$(".banner_cont_inner > li").outerWidth(emtWidth);
	$(".banner_cont_inner > li").outerHeight(emtHeight);

	$(".banner_page span:first-child").addClass("on");
	$(".banner_cont_inner li:first-child").addClass("carrent");
	var emtWidth = $(".banner_cont_inner li").outerWidth();
	var sumWidth = emtWidth * $(".banner_cont_inner li").length;
	var listEnt = $(".banner_cont_inner li").length;
	$(".banner_cont_inner").outerWidth(sumWidth)

	$(".banner_page").each(function() {
		$(".banner_page a").click(function(){
			$(".banner_page span").removeClass("on");
			$(this).parent().addClass("on");
			var pageList = $(".banner_page span.on").index();
			$(".banner_cont_inner li").removeClass("carrent");
			$(".banner_cont_inner li").eq(pageList).addClass("carrent");
			carrentWidth = emtWidth * ($(".banner_cont_inner li.carrent").index());
			var listindex = $(".banner_cont_inner li.carrent").index();
			$(".banner_cont_inner").not(":animated").animate({left:0 - carrentWidth})
		});
	});

	$(".banner_cont_inner").each(function() {
		$(".next a").click(function(){
			var listindex = $(".banner_cont_inner li.carrent").index();
			carrentWidth = emtWidth * ($(".banner_cont_inner li.carrent").index() + 1);

			if (listindex < (listEnt -1)){
				$(".banner_cont_inner").children(".carrent").next().addClass("carrent");
				$(".banner_cont_inner").children(".carrent").prev().removeClass("carrent");
				$(".banner_page").children(".on").next().addClass("on");
				$(".banner_page").children(".on").prev().removeClass("on");
				$(".banner_cont_inner").not(":animated").animate({left:0 - carrentWidth})
			}

			if (listindex == (listEnt - 1)){
				$(".banner_cont_inner").children(".carrent").removeClass("carrent");
				$(".banner_cont_inner li").eq(0).addClass("carrent");
				$(".banner_page").children(".on").removeClass("on");
				$(".banner_page span").eq(0).addClass("on");
				$(".banner_cont_inner").not(":animated").animate({left:0})
			}
		});

		$(".prev a").click(function(){
			var listindex = $(".banner_cont_inner li.carrent").index()
			carrentWidth = emtWidth * ($(".banner_cont_inner li.carrent").index() - 1);

			if (listindex > 0){
				$(".banner_cont_inner").children(".carrent").prev().addClass("carrent");
				$(".banner_cont_inner").children(".carrent").next().removeClass("carrent");
				$(".banner_page").children(".on").prev().addClass("on");
				$(".banner_page").children(".on").next().removeClass("on");
				$(".banner_cont_inner").not(":animated").animate({left: 0 + (-carrentWidth)})
			}

			if (listindex == 0){
				$(".banner_cont_inner").children(".carrent").removeClass("carrent");
				$(".banner_cont_inner li").eq(listEnt - 1).addClass("carrent");
				$(".banner_page").children(".on").removeClass("on");
				$(".banner_page span").eq(listEnt - 1).addClass("on");
				$(".banner_cont_inner").not(":animated").animate({left:0 - (emtWidth * 3)})
			}
		});


		var speed = 4000;
		var timer = null;
		var auto = true;
		var cnt = 1;

		if(auto) timer = setInterval(autoSlide, speed);
		$(".wrap_banner").bind({
			'mouseenter': function(){
				if(!auto) return false;
				clearInterval(timer);
				auto = false;
			},
			'mouseleave': function(){
				timer = setInterval(autoSlide, speed);
				auto = true;
			}
		})
		
		
		function autoSlide(){
			var listindex = $(".banner_cont_inner li.carrent").index();
			carrentWidth = emtWidth * ($(".banner_cont_inner li.carrent").index() + 1);
			if (listindex < (listEnt -1)){
				$(".banner_cont_inner").children(".carrent").next().addClass("carrent");
				$(".banner_cont_inner").children(".carrent").prev().removeClass("carrent");
				$(".banner_page").children(".on").next().addClass("on");
				$(".banner_page").children(".on").prev().removeClass("on");
				$(".banner_cont_inner").not(":animated").animate({left:0 - carrentWidth})
			}

			if (listindex == (listEnt - 1)){
				$(".banner_cont_inner").children(".carrent").removeClass("carrent");
				$(".banner_cont_inner li").eq(0).addClass("carrent");
				$(".banner_page").children(".on").removeClass("on");
				$(".banner_page span").eq(0).addClass("on");
				$(".banner_cont_inner").not(":animated").animate({left:0})
			}
		}
		

	});
});





});