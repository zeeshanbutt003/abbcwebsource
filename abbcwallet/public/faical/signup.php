<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Signup to your account</title>
    <style type="text/css">
        body { font-family: Helvetica, sans-serif; }
        h2, h3 { margin-top:0; }
        form { margin-top: 15px; }
        form > input { margin-right: 15px; }
       #results{
            width: 320px;
            height: 240px;
            border: 1px solid;
            background:#ccc;
            text-align:center;
            margin-top: auto;
            margin-bottom: auto;
        }

        #my_camera {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        #results {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8" style="text-align: center">
                <h1>Signup to your account with webcam</h1>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
                <div id="my_camera"></div>

                <form id="form">
                    <button class="btn btn-primary" type = "button" onClick = "take_snapshot()" style="margin-top: 10px">Take Snapshot</button>
                    <button class="btn btn-primary" type = "submit" id = "signup">Signup with image</button>
                </form>
            </div>
            <div class="col-sm-5" >
                <div id="results">Your captured image will appear here...</div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>


    <!-- First, include the Webcam.js JavaScript Library -->
    <script type="text/javascript" src="webcam.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Configure a few settings and attach camera -->
    <script language="JavaScript">
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90,
            force_flash:false
        });
        Webcam.attach( '#my_camera' );
    </script>

        <!-- Code to handle taking the snapshot and displaying it locally -->
    <script language="JavaScript">
        var base64 = '';
        function take_snapshot() {
            // take snapshot and get image data

            Webcam.snap( function(data_uri) {
                console.log(data_uri);
                $("#image").val(data_uri);
                base64 = data_uri;
                // display results in page
                // document.getElementById('results').innerHTML =
                //     '<h2>Here is your image:</h2>' +
                //     '<img src="'+data_uri+'"/>';
                document.getElementById('results').innerHTML =
                    '<img src="'+data_uri+'" align="center"/>';
            } );
        }
        $("#form").submit(function (e) {
           signup();
           e.preventDefault();
        });
        function signup() {
            var request = $.ajax({
                url: "enroll.php",
                type: "POST",
                data: {image : base64,username:$("#username").val()},
                success:function (response) {
                    console.log(response)
                    response = JSON.parse(response);
                    if(response.hasOwnProperty('Errors')){
                        if(response.Errors.length >0){
                            var code = response.Errors[0].ErrCode;
                            if(code == 1002){
                                alert("take a snapshot before login");
                            }
                            if(code == 1005){
                                alert("face is already registered");
                            }
                            if(code == 5002){
                                alert("no faces found in the image");
                            }
                        }
                    }else {
                        alert("successfully signup");
                    }

                },
                error:function (error) {
                    console.log(error);
                }

            });
        }
    </script>
</body>
</html>
