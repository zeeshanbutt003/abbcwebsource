<?php





$image = $_POST['image'];
$name = rand(10000000,99999999);

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.kairos.com/recognize",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\n    \"image\":\"".$image."\",\n    \"gallery_name\":\"new\"\n}",
    CURLOPT_HTTPHEADER => array(
        "app_id: 6cee05e7",
        "app_key: 122f1ca32b068f4b8bd4f6a28bb680ec",
        "cache-control: no-cache",
        "content-type: application/json",
        "postman-token: a26db98a-3875-5dff-1745-a395eb6607eb"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

$response = json_decode($response,true);
curl_close($curl);

if($response['images'][0]['transaction']['status'] == 'failure'){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.kairos.com/enroll",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n    \"image\":\"".$image."\",\n    \"subject_id\":\"".$name."\",\n    \"gallery_name\":\"new\"\n}",
        CURLOPT_HTTPHEADER => array(
            "app_id: 6cee05e7",
            "app_key: 122f1ca32b068f4b8bd4f6a28bb680ec",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 3d887db0-7ed2-b501-3481-936a49a88e55"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }
}else{
    echo json_encode(array('Errors'=>[array('ErrCode'=>'1005')]));
}



